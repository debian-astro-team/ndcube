ndcube (2.3.1-1) unstable; urgency=medium

  * New upstream version 2.3.1
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Sat, 15 Feb 2025 21:25:24 +0100

ndcube (2.3.0-1) unstable; urgency=medium

  * New upstream version 2.3.0
  * Rediff patches. Drop astropy-7 dependency patch: applied upstream
  * Don't install examples gallery
  * New build dependency python3-pytest-mpl

 -- Ole Streicher <olebole@debian.org>  Sat, 18 Jan 2025 16:08:51 +0100

ndcube (2.2.4-2) unstable; urgency=medium

  * Fix Astropy 7.0 version dependency

 -- Ole Streicher <olebole@debian.org>  Tue, 26 Nov 2024 08:51:40 +0100

ndcube (2.2.4-1) unstable; urgency=medium

  * New upstream version 2.2.4
  * Rediff patches
  * Set minversion for sunpy-shinx-theme build dependency

 -- Ole Streicher <olebole@debian.org>  Thu, 24 Oct 2024 17:25:36 +0200

ndcube (2.2.2-1) unstable; urgency=medium

  * Put myself onto the list of uploaders
  * New upstream version 2.2.2
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Fri, 13 Sep 2024 09:58:57 +0200

ndcube (2.2.1-1) unstable; urgency=medium

  * Team upload
  * Add debian/gitlab-ci.yml
  * New upstream version 2.2.1
  * Rediff patches
  * Push Standards-Version to 4.7.0. No changes needed
  * New test dependency python3-specutils
  * Mark one test as remote

 -- Ole Streicher <olebole@debian.org>  Thu, 05 Sep 2024 21:15:42 +0200

ndcube (2.2.0-1) unstable; urgency=medium

  * Team upload
  * New upstream version 2.2.0
  * Rediff patches
  * Ignore all Astropy warnings (Closes: #1055877)
  * Remove outdated pyton3-astropy-helpers build dependency
  * Remove outdated versionized build dependencies
  * New build dependency python3-sphinxext-opengraph
  * Push Standards-Version to 4.6.2. No changes needed.

 -- Ole Streicher <olebole@debian.org>  Sat, 03 Feb 2024 17:20:45 +0100

ndcube (2.0.3-1) unstable; urgency=medium

  * Team upload
  * Remove pgpsig requirement from d/watch
  * New upstream version 2.0.3
  * Rediff patches
  * Ignore all deprecation warnings (Closes: #1030490)
  * Fix compatibility with reproject 0.10

 -- Ole Streicher <olebole@debian.org>  Wed, 08 Feb 2023 09:36:54 +0100

ndcube (2.0.2-2) unstable; urgency=medium

  * Team upload
  * Ignore Astropy deprecations warnings and FutureWarnings (Closes: #1020121)
  * Fix extra coords test

 -- Ole Streicher <olebole@debian.org>  Mon, 24 Oct 2022 17:17:54 +0200

ndcube (2.0.2-1) unstable; urgency=medium

  * New upstream release
  * Replace jquery, bootstrap, and bootswatch static files with symlinks
  * Update Standards-Version to 4.6.1 (no change needed)
  * Update debian/copyright
  * Remove duplicates in documentation package

 -- Vincent Prat <vivi@debian.org>  Tue, 23 Aug 2022 13:11:16 +0200

ndcube (2.0.1-1) unstable; urgency=medium

  * New upstream release
  * Update patch ignore_sphinx_changelog
  * Add build and test dependency on python3-mpl-animators

 -- Vincent Prat <vivi@debian.org>  Fri, 12 Nov 2021 15:10:24 +0100

ndcube (2.0.0~rc2-1) unstable; urgency=medium

  * New upstream release

 -- Vincent Prat <vivi@debian.org>  Wed, 20 Oct 2021 19:54:52 +0200

ndcube (2.0.0~rc1-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).

 -- Vincent Prat <vivi@debian.org>  Sat, 25 Sep 2021 11:42:17 +0200

ndcube (2.0.0~rc1-4) unstable; urgency=medium

  * Add test dependency on reproject

 -- Vincent Prat <vivi@debian.org>  Thu, 16 Sep 2021 23:52:17 +0200

ndcube (2.0.0~rc1-3) unstable; urgency=medium

  * Add test dependency on sunpy

 -- Vincent Prat <vivi@debian.org>  Wed, 15 Sep 2021 19:04:28 +0200

ndcube (2.0.0~rc1-2) unstable; urgency=medium

  * Add test dependency on matplotlib

 -- Vincent Prat <vivi@debian.org>  Tue, 14 Sep 2021 20:57:43 +0200

ndcube (2.0.0~rc1-1) unstable; urgency=medium

  * New upstream release
  * Temporary patch to ignore dependency on sphinx-changelog
  * Remove obsolete patchs remove_non_ascii_chars and use_system_helpers
  * Add dependency on python3-gwcs and python3-reproject
  * Auto clean generated _version.py file
  * Update d/copyright
  * Replace Homepage field in d/control
  * Update Standards-Version to 4.6.0.1 (no change needed)

 -- Vincent Prat <vivi@debian.org>  Wed, 08 Sep 2021 19:18:31 +0200

ndcube (1.4.2-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards-Version to 4.5.1

 -- Vincent Prat <vivi@debian.org>  Fri, 20 Nov 2020 19:06:27 +0100

ndcube (1.4.1-1) unstable; urgency=medium

  * New upstream release

 -- Vincent Prat <vivi@debian.org>  Wed, 04 Nov 2020 18:04:18 +0100

ndcube (1.4.0-1) unstable; urgency=medium

  * New upstream release

 -- Vincent Prat <vivi@debian.org>  Tue, 03 Nov 2020 11:38:50 +0100

ndcube (1.3.3-1) unstable; urgency=medium

  * New upstream release

 -- Vincent Prat <vivi@debian.org>  Sun, 18 Oct 2020 20:35:22 +0200

ndcube (1.3.2-2) unstable; urgency=medium

  * Add parfive to the test dependencies (Closes: #963954)
  * Bump debhelper compatibility version to 13
  * Change back to the older maintainer email address

 -- Vincent Prat <vivi@debian.org>  Sat, 04 Jul 2020 09:08:25 +0200

ndcube (1.3.2-1) unstable; urgency=medium

  * New upstream release
  * Update maintainer email address

 -- Vincent Prat <vinceprat@free.fr>  Thu, 23 Apr 2020 16:53:50 +0200

ndcube (1.3.1-1) unstable; urgency=medium

  * New upstream release
  * Add minimum version for dependency python3-sphinx

 -- Vincent Prat <vinceprat@free.fr>  Sun, 19 Apr 2020 10:20:40 +0200

ndcube (1.3.0-2) unstable; urgency=medium

  * Fix autopkgtest

 -- Vincent Prat <vinceprat@free.fr>  Fri, 17 Apr 2020 15:43:14 +0200

ndcube (1.3.0-1) unstable; urgency=medium

  * New upstream version
  * Drop/update outdated patches
  * Add dependency on python3-setuptools-scm
  * Update Standards-Version to 4.5.0
  * Add Rules-Requires-Root statement
  * Remove d/compat file and add dependency on debhelper-compat

 -- Vincent Prat <vinceprat@free.fr>  Wed, 15 Apr 2020 18:52:26 +0200

ndcube (1.1.3-2) unstable; urgency=medium

  * Patch to disable tests that fail with sunpy 1.X

 -- Vincent Prat <vinceprat@free.fr>  Wed, 04 Sep 2019 10:28:53 +0200

ndcube (1.1.3-1) unstable; urgency=medium

  * New upstream release
  * New patch fix_extent_test.patch to fix plotting test
  * Update debhelper compatibility version to 12
  * Update Standards-Version to 4.4.0

 -- Vincent Prat <vinceprat@free.fr>  Sun, 28 Jul 2019 18:33:02 +0200

ndcube (1.1.1-1) unstable; urgency=medium

  * New upstream release
  * Add two dependencies
  * Update d/copyright
  * Update Standards-Version to 4.3.0

 -- Vincent Prat <vinceprat@free.fr>  Sat, 19 Jan 2019 13:45:01 +0100

ndcube (1.0.1-2) unstable; urgency=medium

  * Fix CI tests
  * Update Standards-Version to 4.2.1
  * Use a minimal upstream signing key
  * Remove useless data directory

 -- Vincent Prat <vinceprat@free.fr>  Mon, 19 Nov 2018 21:52:47 +0000

ndcube (1.0.1-1) unstable; urgency=medium

  * Initial release. (Closes: #895013)

 -- Vincent Prat <vinceprat@free.fr>  Fri, 06 Apr 2018 11:28:46 +0100
